﻿Template.categories.onCreated () ->
	

  @subscribe 'categories'

Template.categories.helpers
  getCategories: ()->
    return db.categories.find()

Template.categories.events
  'click .new-item-action': (e,t) ->
    console.log 'clicked'
    $name = $('.category-name')
    category={
      'name':$name.val()
      }
    db.categories.insert(category)
    $name.val('')
  'keyup .category-name':(e,t) ->
    if e.which== 13
      console.log 'clicked'
      $name = $('.category-name')
      category={
        'name':$name.val()
        }
      $name.val('')
      db.categories.insert(category)
  'click [data-action=remove-item]': (e,t) ->
    name = e.currentTarget.value
    id = db.categories.findOne('name':name)._id
    db.categories.remove(id)