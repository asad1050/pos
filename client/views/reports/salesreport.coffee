﻿Template.salesreport.onCreated () ->
  t = @
  t.list = new ReactiveVar([]) 
  t.totalsales = new ReactiveVar([])
  t.totalprofit = new ReactiveVar([])
  
  
  	
  t.list.set(db.pos.find())
  
  
  @subscribe 'pos'

Template.salesreport.onRendered ->
  
  $('#dateFrom').datepicker({ format: 'mm-dd-yyyy' })
  $('#dateTo').datepicker({ format: 'mm-dd-yyyy' })  

Template.salesreport.helpers
  getSales: ()->
    t = Template.instance()
    return t.list.get()
  getTotalSales: ()->
    t = Template.instance()
    return t.totalsales.get()
  getTotalProfit: ()->
    t = Template.instance()
    return t.totalprofit.get()


Template.salesreport.events
  'click .search-action':(e,t) ->
    e.preventDefault()
    e.stopPropagation()
    if  $('#dateFrom').val()== '' or $('#dateTo').val() == '' 
      Bert.alert('Please, choose date','warning')
      return
    start = new Date($('#dateFrom').val())
    end = new Date($('#dateTo').val())
    
    start = start.toISOString()
    end = end.toISOString()
    t.list.set(db.pos.find({ 'date': { $gte: start, $lt: end}}))
    list = db.pos.find({ 'date': { $gte: start, $lt: end}})
    console.log start
    t.totalsales.set([])
    t.totalprofit.set([])
    
   
  'click .calculate-action': (e,t) ->
    tsale = 0
    tprofit = 0
    $('.sale').each(()-> 
      value= $(this).text()
      if !isNaN(value) and value.length!=0
        tsale+=parseFloat(value))
    $('.profit').each(()-> 
      value= $(this).text()
      if !isNaN(value) and value.length!=0
        tprofit+=parseFloat(value))
    
    console.log tsale
    console.log tprofit
    if tsale == 0
      tsale = '0'
    if tprofit == 0
      tprofit = '0'
    t.totalsales.set(tsale)
    t.totalprofit.set(tprofit)
    


    
    