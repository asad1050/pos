﻿Template.expensereport.onCreated () ->
  t = @
  t.list = new ReactiveVar([]) 
  t.totalexpenses = new ReactiveVar([])
  
  
  	
  t.list.set(db.expenses.find())
  
  
  @subscribe 'pos'

Template.expensereport.onRendered ->
  $('#dateFrom').datepicker({ format: 'mm-dd-yyyy' })
  $('#dateTo').datepicker({ format: 'mm-dd-yyyy' })  

Template.expensereport.helpers
  getExpenses: ()->
    t = Template.instance()
    return t.list.get()
  getTotalExpenses: ()->
    t = Template.instance()
    return t.totalexpenses.get()


Template.expensereport.events
  'click .search-action':(e,t) ->
    e.preventDefault()
    e.stopPropagation()
    if  $('#dateFrom').val()== '' or $('#dateTo').val() == '' 
      Bert.alert('Please, choose date','warning')
      return
    start = new Date($('#dateFrom').val())
    end = new Date($('#dateTo').val())
    
    start = start.toISOString()
    end = end.toISOString()
    t.list.set(db.expenses.find({ 'date': { $gte: start, $lt: end}}))
    list = db.expenses.find({ 'date': { $gte: start, $lt: end}})
    console.log start
    t.totalexpenses.set([])
    
    console.log t.list.get()
    
  'click .calculate-action': (e,t) ->
    texpense = 0
    
    $('.amount').each(()-> 
      value= $(this).text()
      if !isNaN(value) and value.length!=0
        texpense+=parseFloat(value))
    if texpense == 0
      texpense = '0'
    
    t.totalexpenses.set(texpense) 


    
    
    