﻿Template.expenses.onCreated () ->
  	

  @subscribe 'expenses'
  

Template.expenses.helpers
  getExpenses: ()->
    return db.expenses.find()

Template.expenses.events
  'click .new-expense-action':(e,t) ->
    e.preventDefault()
    e.stopPropagation()
    $name= $('.new-expense-name')
    $amount= $('.new-expense-amount')
   
    if $name.val() == '' or $amount.val() == '' 
      Bert.alert('All fields must be filled', 'warning');
      console.log('error')
      return
    amount = parseFloat($amount.val())
    name = $name.val()
    now = moment() 
    dt = now.format('MM-DD-YYYY')
    date = new Date(dt)  
    expense={
        'name':name
        'amount':amount
        'date': date.toISOString()
    }
    db.expenses.insert(expense);
    console.log 'success'
    $amount.val('')
    $name.val('')
    $name.focus()
    Bert.alert('Success', 'success')
  'keyup .new-expense-amount': (e,t) ->
    
    if e.which == 13
      e.preventDefault()
      e.stopPropagation()
      $name= $('.new-expense-name')
      $amount= $('.new-expense-amount')
   
      if $name.val() == '' or $amount.val() == '' 
        Bert.alert('All fields must be filled', 'warning');
        console.log('error')
        return
      amount = parseFloat($amount.val())
      name = $name.val()
      now = moment() 
      dt = now.format('MM-DD-YYYY')
      date = new Date(dt)   
      expense={
          'name':name
          'amount':amount
          'date': date.toISOString()
      }
      db.expenses.insert(expense);
      console.log 'success'
      $amount.val('')
      $name.val('')
      $name.focus()
      Bert.alert('Success', 'success')     


  'click [data-action=remove-item]': (e,t) ->
    e.preventDefault()
    e.stopPropagation()
    id = e.currentTarget.value
    db.expenses.remove(id)
    Bert.alert('Success','success')