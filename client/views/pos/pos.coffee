Template.pos.onCreated () ->
  t = @
  t.listedItems = new ReactiveVar([])
  t.profit = new ReactiveVar([])
  t.id = new ReactiveVar([])
  t.choice = new ReactiveVar([])
  t.compensate = new ReactiveVar([])
  t.nr = new ReactiveVar([])
  t.srid = new ReactiveVar([])
  t.srdate = new ReactiveVar([])
  
  t.id.set('0')
  @subscribe 'pos'
  @subscribe 'items'

Template.pos.onRendered () ->
  $('#invoicenr-sec').hide()
  $('#upd-trans').hide() 
  $('.pos-war').hide()


   

Template.pos.helpers
  getListedItems: ()->
    t = Template.instance()
    return t.listedItems.get()
  getList: ()->
    return db.items.find()


Template.pos.events
  'click .btn-add-item-quantity': (e,t) ->
    e.preventDefault()
    e.stopPropagation()
     
      
    console.log 'start'
    $code = $('.item-codeq')
    $quantity = $('.item-quantity')
    console.log 'ok'
    if $code.val() == ''
      Bert.alert('Please, enter code for item', 'warning');
      console.log('error')
      return
    if $quantity.val() == ''
      Bert.alert('Please, enter quantity for item', 'warning');
      console.log('error')
      return
    items = db.items.findOne({'code':$code.val()})
    if items == undefined
      Bert.alert('Incorrect item code', 'warning');
      console.log('error')
      return
    
    if items.quantity - parseFloat($quantity.val())  < 1
      Bert.alert('Required stock for that item is not available','danger')
      return
    console.log items.name
    quantity = parseFloat($quantity.val()) || 1
    list = t.listedItems.get()
    console.log 'list'
    console.log list.length
    profit = t.profit.get()
    id = t.id.get()
    id = id*1 + 1
    t.id.set(id)
    console.log id
    console.log items.name
    if t.compensate.get()
    
      qty = $quantity.val()
      qty = parseFloat(qty) + 0.015
      tempprofit = parseFloat(items.price * parseFloat(qty)) - parseFloat(items.cost * parseFloat(qty))
      list.push({
      'id':id
      'code':$code.val()
      'item': items.name
      'quantity': qty
      'unit': items.price
      'pricesum': parseFloat(items.price) * parseFloat(qty)
      })
      nquan = items.quantity - qty
      db.items.update(items._id,{ $set: {'quantity':nquan}})
    else
      tempprofit = parseFloat(items.price * parseFloat($quantity.val())) - parseFloat(items.cost * parseFloat($quantity.val()))
      list.push({
      'id':id
      'code':$code.val()
      'item': items.name
      'quantity': $quantity.val()
      'unit': items.price
      'pricesum': parseFloat(items.price) * parseFloat($quantity.val())
      
      })
      nquan = items.quantity - $quantity.val()
      db.items.update(items._id,{ $set: {'quantity':nquan}})
    
    
    tmp = profit*1 + parseFloat(tempprofit)
    profit = tmp
    
    
    console.log 'pass'
    t.listedItems.set(list)
    t.profit.set(tmp)
    $code.val('')
    $quantity.val('')
    $('.item-codeq').focus()
    console.log profit
    $('.pos-war').show()
    $('.navbar-default').hide()
    
    

    console.log 'list'
    console.log list.length
  
  'keydown .item-quantity':(e,t) ->
    if e.which == 13
      e.preventDefault()
      e.stopPropagation()
      
      console.log 'start'
      $code = $('.item-codeq')
      $quantity = $('.item-quantity')
      console.log 'ok'
      if $code.val() == ''
        Bert.alert('Please, enter code for item', 'warning');
        console.log('error')
        return
      if $quantity.val() == ''
        Bert.alert('Please, enter quantity for item', 'warning');
        console.log('error')
        return
      items = db.items.findOne({'code':$code.val()})
      if items == undefined
        Bert.alert('Incorrect item code', 'warning');
        console.log('error')
        return
      if items.quantity - parseFloat($quantity.val())  < 1
        Bert.alert('Required stock for that item is not available','danger')
        return
      
      console.log items.name
      quantity = parseFloat($quantity.val()) || 1
      list = t.listedItems.get()
      profit = t.profit.get()
      id = t.id.get()
      id = id*1 + 1
      t.id.set(id)
      console.log id
      console.log items.name
      if t.compensate.get()
    
        qty = $quantity.val()
        qty = parseFloat(qty) + 0.015
        tempprofit = parseFloat(items.price * parseFloat(qty)) - parseFloat(items.cost * parseFloat(qty))
        list.push({
        'id':id
        'code':$code.val()
        'item': items.name
        'quantity': qty
        'unit': items.price
        'pricesum': parseFloat(items.price) * parseFloat(qty)
        })
        nquan = items.quantity - qty
        db.items.update(items._id,{ $set: {'quantity':nquan}})
      else
        tempprofit = parseFloat(items.price * parseFloat($quantity.val())) - parseFloat(items.cost * parseFloat($quantity.val()))
        list.push({
        'id':id
        'code':$code.val()
        'item': items.name
        'quantity': $quantity.val()
        'unit': items.price
        'pricesum': parseFloat(items.price) * parseFloat($quantity.val())
        
        })
        nquan = items.quantity - $quantity.val()
        db.items.update(items._id,{ $set: {'quantity':nquan}})
      tmp = profit*1 + parseFloat(tempprofit)
      profit = tmp
      
      t.listedItems.set(list)
      t.profit.set(tmp)
      $code.val('')
      $quantity.val('')
      $('.item-codeq').focus()
      console.log profit   
      $('.pos-war').show()
      $('.navbar-default').hide() 
 
  'click .btn-add-item-price': (e,t) ->
    e.preventDefault()
    e.stopPropagation()

    $code = $('.item-codep')
    $price = $('.item-price')
    if $code.val() == '' or $price == ''
      Bert.alert('Please, enter price and code in Sale by Price fields', 'warning');
      console.log('error')
      return
    items = db.items.findOne({'code':$code.val()})
    if items == undefined
      Bert.alert('Incorrect item code', 'warning');
      console.log('error')
      return
    
    
    console.log items 
    price = parseFloat($price.val())
    console.log items.price
    quantity = price / parseFloat(items.price)
    console.log 'quantity'
    console.log quantity
    if items.quantity - parseFloat(quantity)  < 1
      Bert.alert('Required stock for that item is not available','danger')
      return
    list = t.listedItems.get()
    profit = t.profit.get()
    id = t.id.get()
    id = id*1 + 1
    t.id.set(id)
    console.log id
    console.log items.name
    if t.compensate.get()
    
        qty = quantity
        qty = parseFloat(qty) + 0.015
        tempprofit = parseFloat(items.price * parseFloat(qty)) - parseFloat(items.cost * parseFloat(qty))
        list.push({
        'id':id
        'code':$code.val()
        'item': items.name
        'quantity': qty
        'unit': items.price
        'pricesum': parseFloat(items.price) * parseFloat(qty)
        })
        nquan = items.quantity - qty
        db.items.update(items._id,{ $set: {'quantity':nquan}}) 
    else
      tempprofit = parseFloat(items.price * parseFloat(quantity)) - parseFloat(items.cost * parseFloat(quantity))
      list.push({
      'id':id
      'code':$code.val()
      'item': items.name
      'quantity': quantity
      'unit': items.price
      'pricesum': parseFloat(items.price) * parseFloat(quantity)
      })
      nquan = items.quantity - quantity
      db.items.update(items._id,{ $set: {'quantity':nquan}}) 
    tmp = profit*1 + parseFloat(tempprofit)
    profit = tmp
       
    t.listedItems.set(list)
    t.profit.set(tmp)
    $code.val('')
    $price.val('')
    $('.item-codep').focus()
    console.log profit
    console.log list
    $('.pos-war').show()
    $('.navbar-default').hide() 

  'keyup .item-price':(e,t) ->
    if e.which == 13
      e.preventDefault()
      e.stopPropagation()
  
      $code = $('.item-codep')
      $price = $('.item-price')
      if $code.val() == '' or $price == ''
        Bert.alert('Please, enter price and code in Sale by Price fields', 'warning');
        console.log('error')
        return
      
      items = db.items.findOne({'code':$code.val()})
      if items == undefined
        Bert.alert('Incorrect item code', 'warning');
        console.log('error')
        return
      
    
      console.log 'clicked'
      console.log items 
      price = parseFloat($price.val())
      console.log items.price
      quantity = price / parseFloat(items.price)
      if items.quantity - parseFloat(quantity)  < 1
        Bert.alert('Required stock for that item is not available','danger')
        return
      console.log 'quantity'
      console.log quantity
      list = t.listedItems.get()
      profit = t.profit.get()
      id = t.id.get()
      id = id*1 + 1
      t.id.set(id)
      console.log id
      console.log items.name
      if t.compensate.get()
    
          qty = quantity
          qty = parseFloat(qty) + 0.015
          tempprofit = parseFloat(items.price * parseFloat(qty)) - parseFloat(items.cost * parseFloat(qty))
          list.push({
          'id':id
          'code':$code.val()
          'item': items.name
          'quantity': qty
          'unit': items.price
          'pricesum': parseFloat(items.price) * parseFloat(qty)
          })
          nquan = items.quantity - qty
          db.items.update(items._id,{ $set: {'quantity':nquan}}) 
      else
        tempprofit = parseFloat(items.price * parseFloat(quantity)) - parseFloat(items.cost * parseFloat(quantity))
        list.push({
        'id':id
        'code':$code.val()
        'item': items.name
        'quantity': quantity
        'unit': items.price
        'pricesum': parseFloat(items.price) * parseFloat(quantity)
        })
        nquan = items.quantity - quantity
        db.items.update(items._id,{ $set: {'quantity':nquan}}) 
      tmp = profit*1 + parseFloat(tempprofit)
      profit = tmp
      
      t.listedItems.set(list)
      t.profit.set(tmp)
      $code.val('')
      $price.val('')
      $('.item-codep').focus()
      $('.pos-war').show()
      $('.navbar-default').hide() 
      console.log profit  
        


  'click [data-action=remove-item]': (e, t) ->
    e.preventDefault()
    e.stopPropagation()
    console.log e
    quantity = e.currentTarget.getAttribute('data-quan')
    if quantity == null
      Bert.alert('Please, repress remove button', 'warning')
      return
    
    Id = e.currentTarget.value
    
    console.log Id
    list = t.listedItems.get()
    console.log list
    len = (Object.keys(list).length )
    indx = 0
    console.log len
    while (parseInt(indx) < parseInt(len))
      console.log 'loop'
      console.log list[indx].id
      if  list[indx].id == parseInt(Id)
        Id = indx
        console.log 'found'   
        console.log indx
        break
      indx+=1
    
    console.log 'Id'
    console.log Id
    items = list[Id]
    list.splice(Id,1)
    t.listedItems.set(list) 
    profit = t.profit.get()
    console.log items
    cost = db.items.findOne({'code':items.code})
    qty = cost.quantity
    cost = cost.cost 
    tempprofit = parseFloat(items.pricesum) - parseFloat(cost * parseFloat(items.quantity))
    tmp = profit*1 - parseFloat(tempprofit)
    profit = tmp
    nquan = qty*1 + parseFloat(quantity)
    console.log nquan
    item = db.items.findOne({'code':items.code})
    console.log item
    db.items.update(item._id,{ $set: {'quantity':nquan}})   
    t.listedItems.set(list)
    t.profit.set(tmp)
    console.log 'profit'
    console.log profit 
    
    if list.length >= 1
      $('.pos-war').show()
      $('.navbar-default').hide()
    else
      $('.pos-war').hide()
      $('.navbar-default').show() 
     

  'click .btn-item-searchq': (e,t) ->
    
    t.choice.set(true)
    console.log 'true'
  'click .btn-item-searchp': (e,t) ->
    t.choice.set(false)
    console.log 'false'
  'click [data-action=select-item]': (e, t) ->
    e.preventDefault()
    e.stopPropagation()
    console.log 'clicked'
    code = ''                    
    code = e.currentTarget.value
    
     
    console.log code
    choice  = t.choice.get()
    if choice == true
      $('.item-codeq').val(e.currentTarget.value)
      $('.item-codep').val('') 
    else
      $('.item-codep').val(e.currentTarget.value)
      $('.item-codeq').val('')

  'click .btn-complete-transaction': (e,t) ->
    
    e.preventDefault()
    e.stopPropagation()
    list = t.listedItems.get()
    profit = t.profit.get()
    console.log list
    console.log profit
    if list.length == 0 or profit.length == 0
      Bert.alert('Please, add items for transaction','warning');
      console.log 'error' 
      return
    sale = 0
    i = 0
    console.log sale
    while (i< list.length)
      console.log list[i].pricesum
      sale = sale*1 + parseFloat(list[i].pricesum)
      i++
    console.log sale
    
    console.log list
    console.log profit
    Meteor.call 'pos.add', list, profit, sale, (err, res) ->
      if err
        Bert.alert('Unable to compete transactions', 'danger');
      else
        t.listedItems.set([])
        t.profit.set([])
        console.log res
        window.open('/receipt/'+res, '_blank')
        Bert.alert('Transaction Completed','success'); 
    t.listedItems.set([])
    $('.pos-war').hide()
    $('.navbar-default').show()
       
            
    
  'click .chk-comp': (e,t) ->
    if $('.chk-comp').is(':checked')
      t.compensate.set(true)
    else
      t.compensate.set(false)
  'click .btn-sales-return':(e,t) ->
    e.preventDefault()
    e.stopPropagation()
    $('#invoicenr-sec').show() 
    $('#quantity-sec').hide() 
    $('#comp-trans').hide()
    
    
  'click .btn-get-invoice': (e, t) ->
    e.preventDefault()
    e.stopPropagation()
    $invoicenr = $('.sales-return-nr')
    invoice = db.pos.findOne({'nr':$invoicenr.val()}) 
    t.srid.set(invoice._id)
    t.nr.set($invoicenr.val())
    t.srdate.set(invoice.date)
    t.listedItems.set(invoice.list) 
    t.profit.set(invoice.profit) 
    $('#invoicenr-sec').hide() 
    $('#quantity-sec').show() 
    $('#upd-trans').show()
    $('.sales-return-nr').val('')
    if invoice.list.length >= 1
      $('.pos-war').show()
      $('.navbar-default').hide()
    else
      $('.pos-war').hide()
      $('.navbar-default').show() 

  'keyup .sales-return-nr':(e,t)->
    if e.which == 13
      e.preventDefault()
      e.stopPropagation()
      $invoicenr = $('.sales-return-nr')
      invoice = db.pos.findOne({'nr':$invoicenr.val()}) 
      t.srid.set(invoice._id)
      t.nr.set($invoicenr.val())
      t.listedItems.set(invoice.list) 
      t.profit.set(invoice.profit) 
      $('#invoicenr-sec').hide() 
      $('#quantity-sec').show() 
      $('#upd-trans').show()
      $('.sales-return-nr').val('')
      if invoice.list.length >= 1
         $('.pos-war').show()
         $('.navbar-default').hide()
       else
         $('.pos-war').hide()
         $('.navbar-default').show() 
      
  'click .btn-update-transaction':(e,t)->
    e.preventDefault()
    e.stopPropagation()
    list = t.listedItems.get()
    profit = t.profit.get()
    id = t.srid.get()
    nr = t.nr.get()
    date = t.srdate.get()
    console.log list
    console.log profit
    if list.length == 0 or profit.length == 0
      Bert.alert('Please, add items for transaction','warning');
      console.log 'error' 
      return
    sale = 0
    i = 0
    console.log sale
    while (i< list.length)
      console.log list[i].pricesum
      sale = sale*1 + parseFloat(list[i].pricesum)
      i++
    console.log sale
    
    console.log list
    console.log profit
    Meteor.call 'pos.update', list, profit, sale, id, nr, date, (err, res) ->
      if err
        Bert.alert('Unable to Update transactions', 'danger');
      else
        t.listedItems.set([])
        t.profit.set([])
        console.log res
        window.open('/receipt/'+res, '_blank')
        Bert.alert('Transaction Updated','success');
        $('#comp-trans').show()
        $('#upd-trans').hide()
    t.listedItems.set([])
    $('.pos-war').hide()
    $('.navbar-default').show()  
    
         