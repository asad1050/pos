﻿Template.login.onCreated () ->
  	

  @subscribe 'users'

Template.login.onRendered () ->
 

Template.login.events
  'click .login-action': (e,t) ->
    e.preventDefault()
    e.stopPropagation()
    $username=$('.user-username')
    $password=$('.user-password')
    console.log $username.val()
    Meteor.loginWithPassword($username.val(),$password.val())
    console.log Meteor.user()
  'keyup .user-password':(e,t)->
    if e.which == 13
      e.preventDefault()
      e.stopPropagation()
      $username=$('.user-username')
      $password=$('.user-password')
      console.log $username.val()
      Meteor.loginWithPassword($username.val(),$password.val())
      console.log Meteor.user()

