Template.items.onCreated () ->
  	

  @subscribe 'items'
  @subscribe 'categories'

Template.items.onRendered () ->
  $('.update-item-action').hide()    

Template.items.helpers
  getList: ()->
    return db.items.find()
  getCategories: ()->
    return db.categories.find()

Template.items.events
  'click .new-item-action': (e, t) ->
    e.preventDefault()
    e.stopPropagation()
    $code= $('.new-item-code')
    $name= $('.new-item-name')
    $quantity= $('.new-item-quantity')
    $category=$('.new-item-category')
    $cost= $('.new-item-cost')
    $price= $('.new-item-price')
    if $code.val() == '' or $name.val() == '' or $category.val() == '' or $quantity.val() == '' or $cost.val() == '' or $price.val() == ''
      Bert.alert('All fields must be filled', 'warning');
      console.log('error')
      return
    code = $code.val()
    name = $name.val()
    category = $category.val()
    price = parseFloat($price.val())
    cost = parseFloat($cost.val())
    quantity = parseFloat($quantity.val())
    item={
        'code':code
        'name':name
        'quantity':quantity
        'category':category
        'cost':cost
        'price':price
    }
    db.items.insert(item);
    console.log 'success'
    $code.val('')
    $name.val('')
    $quantity.val('')
    $cost.val('')
    $price.val('')
    Bert.alert('Success', 'success');

  'click [data-action=remove-item]':(e,t) ->
    e.preventDefault()
    e.stopPropagation()
    code= e.currentTarget.value
    id = db.items.findOne('code':code)._id
    db.items.remove(id)
    Bert.alert('Removed','success')
  'click [data-action=choose-category]': (e,t) ->
    $('.new-item-category').val(e.currentTarget.innerText)

  'click [data-action=edit-item]':(e,t) ->
    $code= $('.new-item-code')
    $name= $('.new-item-name')
    $quantity= $('.new-item-quantity')
    $category=$('.new-item-category')
    $cost= $('.new-item-cost')
    $price= $('.new-item-price')
    console.log e
    $code.val(e.currentTarget.attributes.itemcode.nodeValue)
    $name.val(e.currentTarget.attributes.itemname.nodeValue)
    $quantity.val(e.currentTarget.attributes.itemquantity.nodeValue)
    $category.val(e.currentTarget.attributes.itemcategory.nodeValue)
    $cost.val(e.currentTarget.attributes.itemcost.nodeValue)
    $price.val(e.currentTarget.attributes.itemprice.nodeValue)
    
    $('.update-item-action').show()
    $('.new-item-action').hide()


  'click .update-item-action': (e,t) ->
    e.preventDefault()
    e.stopPropagation()
    $('.update-item-action').hide()
    $('.new-item-action').show()
    $code= $('.new-item-code')
    $name= $('.new-item-name')
    $quantity= $('.new-item-quantity')
    $category=$('.new-item-category')
    $cost= $('.new-item-cost')
    $price= $('.new-item-price')
    if $code.val() == '' or $name.val() == '' or $category.val() == '' or $quantity.val() == '' or $cost.val() == '' or $price.val() == ''
      Bert.alert('All fields must be filled', 'warning');
      console.log('error')
      return
    code = $code.val()
    name = $name.val()
    category = $category.val()
    price = parseFloat($price.val())
    cost = parseFloat($cost.val())
    quantity = parseFloat($quantity.val())
    item={
        'code':code
        'name':name
        'quantity':quantity
        'category':category
        'cost':cost
        'price':price
    }
    id = db.items.findOne('code':code)._id
    db.items.update(id,item)
    console.log 'success'
    $code.val('')
    $name.val('')
    $quantity.val('')
    $cost.val('')
    $price.val('')
    Bert.alert('Success', 'success');

    
  'click [data-action=reset-item]':(e,t) ->
    e.preventDefault()
    e.stopPropagation()
    code= e.currentTarget.value
    item = db.items.findOne('code':code)
    waste = item.quantity * item.price
    db.items.update(item._id,$set :{'quantity':0})
    now = moment()
    expense={
          'name': item.category + ' | ' + item.name
          'amount':waste
          'date': now.format('DD-MM-YYYY')
      }
    db.expenses.insert(expense)
    
    