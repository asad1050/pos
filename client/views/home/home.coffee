﻿Template.home.onCreated () ->
  t = @
  t.nav = new ReactiveVar('home')  
  t.msg = new ReactiveVar('Ok')	
Template.home.helpers
  isAdmin: ()->
    return Meteor.user().profile.role == 'admin'
  isCashier: ()->
    return Meteor.user().profile.role == 'cashier'
  isCashierplus: ()->
    return Meteor.user().profile.role == 'cashierplus'
  isCategories: ()->
    t = Template.instance()
    return t.nav.get() == 'categories'
  isItems: ()->
    t = Template.instance()
    return t.nav.get() == 'items'
  isUsers: ()->
    t = Template.instance()
    return t.nav.get() == 'users'
  isExpenses: ()->
    t = Template.instance()
    return t.nav.get() == 'expenses'
  isPOS: ()->
    t = Template.instance()
    return t.nav.get() == 'pos'
  isSalesR: ()->
    t = Template.instance()
    return t.nav.get() == 'salesr'
  isExpensesR: ()->
    t = Template.instance()
    return t.nav.get() == 'expensesr'
  isHome: ()->
    t = Template.instance()
    return t.nav.get() == 'home'
Template.home.events
  'click .signout-action':(e,t)->
    Meteor.logout()
  'click .categories-action':(e,t) ->
    t.nav.set('categories')
  'click .items-action':(e,t) ->
    t.nav.set('items')
  'click .users-action':(e,t) ->
    t.nav.set('users')
  'click .expenses-action':(e,t) ->
    t.nav.set('expenses')
  'click .pos-action':(e,t) ->
    t.nav.set('pos')
  'click .expensesr-action':(e,t) ->
    t.nav.set('expensesr')
  'click .salesr-action':(e,t) ->
    t.nav.set('salesr')
  'click .home-action':(e,t) ->
    t.nav.set('home')
