﻿Template.users.onCreated () ->
  	

  @subscribe 'users'
  

Template.users.helpers
  getUsers: ()->
    return db.users.find()

Template.users.events
  'click [data-action=choose-role]': (e,t) ->
    e.preventDefault()
    e.stopPropagation()
    console.log 'clicked'
    $('.new-user-role').val(e.currentTarget.innerText)
  'click .new-user-action':(e,t) ->
    e.preventDefault()
    e.stopPropagation()
    $name= $('.new-user-name')
    $username= $('.new-user-username')
    $password= $('.new-user-password')
    $role= $('.new-user-role')
   
    if $name.val() == '' or $username.val() == '' or $password.val() == '' or $role.val() == '' 
      Bert.alert('All fields must be filled', 'warning');
      console.log('error')
      return
    username = $username.val()
    password = $password.val()
    name = $name.val()
    role = $role.val()
       
    Accounts.createUser({
          'username':username
          'password':password
          'profile':{'role':role,'name':name}
      })
    
    console.log 'success'
    Meteor.logout()
    $username.val('')
    $password.val('')
    $role.val('')
    $name.val('')
    $name.focus()
    Bert.alert('Success', 'success')
  'keyup .new-user-role':(e,t) ->
    
    if e.which == 13
      e.preventDefault()
      e.stopPropagation()
      $name= $('.new-user-name')
      $username= $('.new-user-username')
      $password= $('.new-user-password')
      $role= $('.new-user-role')
   
      if $name.val() == '' or $username.val() == '' or $password.val() == '' or $role.val() == '' 
        Bert.alert('All fields must be filled', 'warning');
        console.log('error')
        return
      if db.users.findOne('username':username) != undefined
        Bert.alert('username already exists','warning')
        return
      username = $username.val()
      password = $password.val()
      name = $name.val()
      role = $role.val()
       
      Accounts.createUser({
          'username':username
          'password':password
          'profile':{'role':role
                     'name':name}
      })
      
      db.users.update(id,$set:{'role':role, 'name':name})
      console.log 'success'
      Meteor.logout()
      $username.val('')
      $password.val('')
      $role.val('')
      $name.val('')
      $name.focus()
      Bert.alert('Success', 'success')  


  'click [data-action=remove-item]': (e,t) ->
    e.preventDefault()
    e.stopPropagation()
    id = e.currentTarget.value
    db.users.remove(id)
    Bert.alert('Success','success')  
