﻿Meteor.methods
  'pos.add': (list, profit, sale) ->
    now = moment()
    dt = now.format('MM-DD-YYYY')
    date = new Date(dt)
    year = now.format('YYYY')
    month = now.format('MM')
    day = now.format('DD')
    hour = now.format('HH')
    min = now.format('mm')
    sec = now.format('SS')
    countStr = '01'
    count = 1
    invoiceNr = "#{year}#{month}#{day}#{hour}#{min}#{sec}"
    
    invoice = {
      'nr': invoiceNr
      'date': date.toISOString()
      'list':list
      'profit':profit
      'sale':sale
      
    }
    db.pos.insert(invoice)
    return invoiceNr
  'pos.update': (list, profit, sale, id, invoicenr, date) ->
    now = moment()
    dt = now.format('MM-DD-YYYY')
    date = new Date(dt)
    
    invoiceNr = invoicenr
    
    invoice = {
      'nr': invoiceNr
      'date': date.toISOString()
      'list':list
      'profit':profit
      'sale':sale
      
    }
    db.pos.update(id,invoice)
    return invoiceNr
    

Meteor.users.allow({  remove:() -> 
  if Meteor.user().profile.role == 'admin'
     return true
  else 
    return false })