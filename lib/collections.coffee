import { Mongo } from 'meteor/mongo';

@db = {}
db.users = Meteor.users
db.items = new Mongo.Collection('Items');
db.pos = new Mongo.Collection('POS');
db.staff = new Mongo.Collection('Staff');
db.expenses = new Mongo.Collection('Expenses');
db.categories = new Mongo.Collection('Categories');
